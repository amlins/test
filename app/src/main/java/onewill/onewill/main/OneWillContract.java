package onewill.onewill.main;

import onewill.onewill.authentication.events.CanceledAuthenticationEvent;
import onewill.onewill.authentication.events.CheckAuthenticationEvent;
import onewill.onewill.communication.api.events.ApiAuthResultEvent;
import onewill.onewill.communication.api.events.ApiRequestAuthEvent;
import onewill.onewill.communication.wifi.events.WifiStatusEvent;
import onewill.onewill.main.events.ShowAuthenticationEvent;
import onewill.onewill.data.events.EndPurchaseEvent;
import onewill.onewill.data.Product;

public class OneWillContract {
    interface View{
        void showProductList();
        void showAuthentication(Product product);
        void back();
        void displayWarning(boolean hide);
    }

    interface Controller {
        void onEvent(EndPurchaseEvent endPurchaseEvent);
        void onEvent(ShowAuthenticationEvent showAuthenticationEvent);
        void onEvent(CanceledAuthenticationEvent canceledAuthenticationEvent);
        void onEvent(WifiStatusEvent event);
        void onDestroy();
    }
}
