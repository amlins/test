package onewill.onewill.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import onewill.onewill.R;
import onewill.onewill.authentication.AuthenticationFragment;
import onewill.onewill.data.Product;
import onewill.onewill.communication.wifi.events.CheckWifiEvent;
import onewill.onewill.productlisting.ProductListingFragment;

public class OneWillActivity extends AppCompatActivity implements OneWillContract.View {

    @Bind(R.id.layout_main)
    View mContentView;
    @Bind(R.id.main_warning_textview)
    TextView warningTextView;

    private ProductListingFragment productListingFragment = new ProductListingFragment();
    private AuthenticationFragment authenticationFragment = new AuthenticationFragment();
    private OneWillController controller = new OneWillController(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        ButterKnife.bind(this);
        checkUI();
        EventBus.getDefault().post(new CheckWifiEvent());

        //first fragment (so far) is product listing
        showProductList();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        controller.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showProductList() {
        replaceFragment(productListingFragment, "productlist");
    }

    @Override
    public void showAuthentication(Product selected) {
        authenticationFragment.setSelectedProduct(selected);
        replaceFragment(authenticationFragment, "authentication");
    }

    public void replaceFragment(Fragment fragment, String name) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.layout_main, fragment);
        transaction.addToBackStack(name);
        transaction.commit();
    }

    @Override
    public void back() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (fragmentManager.getBackStackEntryCount() > 1)
            fragmentManager.popBackStack();
    }

    @Override
    public void displayWarning(boolean hide) {
        warningTextView.setVisibility(hide ? TextView.INVISIBLE : TextView.VISIBLE);
    }



    @Override
    public void onBackPressed() {
        back();
    }

    // Hide the system bars, fullscreen
    private void hideSystemUI() {
        mContentView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);
    }

    public void checkUI() {
        hideSystemUI();
        View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {

                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                            hideSystemUI();
                    }
                });
    }
}
