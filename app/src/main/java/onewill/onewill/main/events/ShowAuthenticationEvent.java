package onewill.onewill.main.events;

import onewill.onewill.data.Product;

public class ShowAuthenticationEvent {
    private Product selected;

    public ShowAuthenticationEvent(Product selected) {
        this.selected = selected;
    }

    public void setSelected(Product selected) {
        this.selected = selected;
    }

    public Product getSelected() {
        return selected;
    }
}
