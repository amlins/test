package onewill.onewill.main;

import de.greenrobot.event.EventBus;
import onewill.onewill.authentication.events.CanceledAuthenticationEvent;
import onewill.onewill.communication.api.events.ApiAuthResultEvent;
import onewill.onewill.communication.api.events.ApiRequestAuthEvent;
import onewill.onewill.communication.wifi.events.WifiStatusEvent;
import onewill.onewill.main.events.ShowAuthenticationEvent;
import onewill.onewill.data.events.EndPurchaseEvent;

public class OneWillController implements OneWillContract.Controller {
    private OneWillContract.View mainView;

    public OneWillController(OneWillContract.View mainView) {
        this.mainView = mainView;
        EventBus.getDefault().register(this);
    }

    @Override
    public void onEvent(EndPurchaseEvent endPurchaseEvent)
    {
        mainView.showProductList();
    }

    @Override
    public void onEvent(ShowAuthenticationEvent showAuthenticationEvent)
    {
        mainView.showAuthentication(showAuthenticationEvent.getSelected());
    }

    @Override
    public void onEvent(CanceledAuthenticationEvent canceledAuthenticationEvent) {
        mainView.back();
    }

    public void onEvent(WifiStatusEvent event){

        mainView.displayWarning(event.isConnected());
    }


    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

}
