package onewill.onewill;

import android.app.Application;

import onewill.onewill.communication.api.ApiService;
import onewill.onewill.communication.wifi.WifiChecker;
import onewill.onewill.data.ProductRepository;

public class OneWillApplication extends Application {

    private ProductRepository productRepository;
    private ApiService apiService;
    private WifiChecker wifiChecker;

    @Override
    public void onCreate() {
        productRepository = new ProductRepository();
        apiService = new ApiService();
        wifiChecker = new WifiChecker(this);
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
