package onewill.onewill.authentication;

import onewill.onewill.communication.api.events.ApiAuthResultEvent;
import onewill.onewill.authentication.events.ErasedPressedEvent;
import onewill.onewill.authentication.events.NumKeyPressedEvent;
import onewill.onewill.communication.api.events.ApiRequestAuthEvent;
import onewill.onewill.data.Product;

public interface AuthenticationContract {
    interface View {
        void updateTextEditor(String value);
        void eraseTextEditor();
        void showResult(boolean result);
        Product getProduct();
        void resetPassword();
        void showProgressBar(boolean visible);
    }

    interface EventHandler {
        void onEvent(NumKeyPressedEvent numKeyPressedEvent);
        void onEvent(ErasedPressedEvent erasedPressedEvent);
        void onEvent(ApiAuthResultEvent apiAuthResultEvent);
        void onDestroy();
    }
}
