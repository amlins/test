package onewill.onewill.authentication;

import de.greenrobot.event.EventBus;
import onewill.onewill.communication.api.events.ApiAuthResultEvent;
import onewill.onewill.authentication.events.ErasedPressedEvent;
import onewill.onewill.authentication.events.NumKeyPressedEvent;
import onewill.onewill.communication.api.events.ApiRequestAuthEvent;
import onewill.onewill.data.events.ServeProductEvent;

public class AuthenticationController implements AuthenticationContract.EventHandler{

    private AuthenticationContract.View view;

    public AuthenticationController(AuthenticationContract.View view) {
        this.view = view;
        EventBus.getDefault().register(this);
    }

    @Override
    public void onEvent(ErasedPressedEvent erasedPressedEvent) {
        view.eraseTextEditor();

    }

    @Override
    public void onEvent(ApiAuthResultEvent apiAuthResultEvent) {

        boolean result = apiAuthResultEvent.isResult();
        view.showProgressBar(false);
        view.showResult(result);
        if(result)
        {
            view.resetPassword();
            EventBus.getDefault().post(new ServeProductEvent(view.getProduct().getId()));
        }
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onEvent(NumKeyPressedEvent numKeyPressedEvent) {
        view.updateTextEditor(numKeyPressedEvent.getValue());
    }


}
