package onewill.onewill.authentication;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import onewill.onewill.R;
import onewill.onewill.authentication.events.CanceledAuthenticationEvent;
import onewill.onewill.communication.api.events.ApiRequestAuthEvent;
import onewill.onewill.data.Product;


public class AuthenticationFragment extends Fragment implements AuthenticationContract.View{

    final int PASSW_LENGTH = 8;
  @Bind(R.id.authentication_recyclerview)
    RecyclerView auth_recyclerview;

    @Bind(R.id.authentication_edittext)
    EditText auth_password;

    @Bind(R.id.auth_product_viewtext)
    TextView productTextView;

    @Bind(R.id.progressBar)
    ProgressBar progressBar;

    @Bind(R.id.cancel_btn)
    Button cancelBtn;

    @OnClick(R.id.cancel_btn) void cancelAuth(){
        auth_password.setText("");
        EventBus.getDefault().post(new CanceledAuthenticationEvent());
    }

    private Product selectedProduct;
    private AuthenticationController controller;

    public Product getSelectedProduct() {
        return selectedProduct;
    }

    public void setSelectedProduct(Product selectedProduct) {
        this.selectedProduct = selectedProduct;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        controller = new AuthenticationController(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.authentication_fragment, container, false);
        ButterKnife.bind(this, rootView);
        if(savedInstanceState != null)
        {
            selectedProduct = (Product) savedInstanceState.getSerializable("selectedProduct");
        }
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3, GridLayoutManager.VERTICAL, true);
        auth_recyclerview.setLayoutManager(gridLayoutManager);
        productTextView.setText(selectedProduct.getTitle());
        initAdapter();
        showProgressBar(false);
        return rootView;
    }

    public void initAdapter(){
        KeyBoardAdapter adapter = new KeyBoardAdapter();
        auth_recyclerview.setAdapter(adapter);
    }

    @Override
    public void updateTextEditor(String val) {
        Editable password = auth_password.getText();
        password.append(val.charAt(0));

        if(password.length() == PASSW_LENGTH){
            showProgressBar(true);

            EventBus.getDefault().post(new ApiRequestAuthEvent(password.toString(),"1"));
        }
    }

    public void eraseTextEditor() {
        Editable password = auth_password.getText();
        int length = password.length();
        if(length > 0)
            password.delete(length-1, length);
        else
            password.clear();
        auth_password.setText(password);
    }

    @Override
    public void showResult(boolean result) {
        Toast toast = Toast.makeText(getContext(), result ? "Produto comprado com sucesso" : "Usuário não encontrado, tente novamente", Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public Product getProduct() {
        return selectedProduct;
    }

    @Override
    public void resetPassword() {
        auth_password.setText("");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        controller.onDestroy();
    }

    @Override
    public void showProgressBar(boolean visible) {
        progressBar.setVisibility(visible ? View.VISIBLE : View.GONE);
        auth_recyclerview.setVisibility(!visible ? View.VISIBLE : View.GONE);
        cancelBtn.setVisibility(!visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("selectedProduct", selectedProduct);
        super.onSaveInstanceState(outState);
    }
}
