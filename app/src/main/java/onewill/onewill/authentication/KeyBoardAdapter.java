package onewill.onewill.authentication;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import onewill.onewill.R;
import onewill.onewill.authentication.events.ErasedPressedEvent;
import onewill.onewill.authentication.events.NumKeyPressedEvent;

public class KeyBoardAdapter extends RecyclerView.Adapter<KeyBoardAdapter.NumKeyViewHolder>{

    @Override
    public NumKeyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.authentication_numkey, parent, false);
        NumKeyViewHolder numKeyViewHolder = new NumKeyViewHolder(v);
        return numKeyViewHolder;
    }

    @Override
    public void onBindViewHolder(NumKeyViewHolder holder, int position) {

        if(position > 2)
            holder.numKey.setText(""+(position-2));

        if(position == 1)
            holder.numKey.setText("0");

        if(position == 0)
        {
            holder.numKey.setEnabled(false);
        }

        if(position == 2)
        {
            holder.numKey.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_backspace_black_24dp,0,0,0);
        }


    }

    @Override
    public int getItemCount() {
        return 12;
    }

    public static class NumKeyViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.auth_numKey_btn)
            Button numKey;

        @OnClick(R.id.auth_numKey_btn) void BtnPressed(){
            String stringValue = numKey.getText().toString();

            if(stringValue != null && !stringValue.isEmpty()){
                EventBus.getDefault().post(new NumKeyPressedEvent(stringValue));
            }
            else
                EventBus.getDefault().post(new ErasedPressedEvent());
        }

       NumKeyViewHolder(View itemView) {
            super(itemView);
           ButterKnife.bind(this, itemView);
        }
    }
}
