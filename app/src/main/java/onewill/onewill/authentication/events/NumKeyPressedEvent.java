package onewill.onewill.authentication.events;

/**
 * Created by abonet on 02/02/16.
 */
public class NumKeyPressedEvent {
    private String value;

    public String getValue() {
        return value;
    }

    public NumKeyPressedEvent(String value) {
        this.value = value;
    }
}
