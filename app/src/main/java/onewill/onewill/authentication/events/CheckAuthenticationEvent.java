package onewill.onewill.authentication.events;

public class CheckAuthenticationEvent {
    private String password;
    private String gym_id ="1";

    public CheckAuthenticationEvent(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }
}
