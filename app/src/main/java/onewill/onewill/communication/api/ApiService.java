package onewill.onewill.communication.api;

import android.util.Log;

import de.greenrobot.event.EventBus;
import onewill.onewill.communication.api.events.ApiAuthResultEvent;
import onewill.onewill.communication.api.events.ApiRequestAuthEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiService {

    private ApiClient client = ServiceGenerator.createService(ApiClient.class);

    public ApiService() {
        EventBus.getDefault().register(this);
    }

    public void onEvent(ApiRequestAuthEvent apiRequestAuthEvent)
    {
        Call<ApiClient.AuthResponse> call = client.authorizeUser(apiRequestAuthEvent.getPassword(), apiRequestAuthEvent.getGym_id());
        call.enqueue(new Callback<ApiClient.AuthResponse>() {
            @Override
           public void onResponse(Response<ApiClient.AuthResponse> response) {
                Log.d("Apiservice", "Response from server " + response.code());
                ApiAuthResultEvent event = new ApiAuthResultEvent(response.code() == 200);
                EventBus.getDefault().post(event);
            }

            @Override
           public void onFailure(Throwable t) {
                Log.d("Apiservice", "Failed to send request to server");
                EventBus.getDefault().post(new ApiAuthResultEvent(false));
            }
       });
   }
}
