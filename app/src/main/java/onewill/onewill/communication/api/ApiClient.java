package onewill.onewill.communication.api;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiClient {
    @GET("/orders/has_user/{password}/0123456789ab")
    Call<AuthResponse> authorizeUser(@Path("password") String password, @Query("gym") String gym_id
    );

    static class AuthResponse{
        String item;
    }
}