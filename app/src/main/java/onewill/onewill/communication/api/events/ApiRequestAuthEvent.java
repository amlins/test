package onewill.onewill.communication.api.events;

public class ApiRequestAuthEvent {
    private String password;
    private String gym_id;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGym_id() {
        return gym_id;
    }

    public void setGym_id(String gym_id) {
        this.gym_id = gym_id;
    }

    public ApiRequestAuthEvent(String password, String gym_id) {
        this.password = password;
        this.gym_id = gym_id;
    }
}
