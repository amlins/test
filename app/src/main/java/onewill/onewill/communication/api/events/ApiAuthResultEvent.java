package onewill.onewill.communication.api.events;

public class ApiAuthResultEvent {
    private boolean result;

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public ApiAuthResultEvent(boolean result) {
        this.result = result;
    }
}
