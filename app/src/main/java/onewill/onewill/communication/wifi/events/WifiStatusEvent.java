package onewill.onewill.communication.wifi.events;


public class WifiStatusEvent {
    private boolean connected;

    public WifiStatusEvent(boolean connected) {
        this.connected = connected;
    }

    public boolean isConnected() {
        return connected;
    }
}
