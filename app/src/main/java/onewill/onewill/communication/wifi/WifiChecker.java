package onewill.onewill.communication.wifi;


import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import de.greenrobot.event.EventBus;
import onewill.onewill.communication.wifi.events.CheckWifiEvent;
import onewill.onewill.communication.wifi.events.WifiStatusEvent;

public class WifiChecker {
    private Application app;

    public WifiChecker(Application app){
        this.app = app;
        respondWifiChange();
    }

    public void onEvent(CheckWifiEvent checkWifiEvent){
        checkWifi();
    }

    /* Register a broadcast receiver to watch for connection changes */
    public void respondWifiChange()
    {
        BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                final String action = intent.getAction();
                Log.d("ConnectionChanged", "" + action.toString());
                if (action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                    checkWifi();
                }
            }
        };
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        app.registerReceiver(receiver, intentFilter);
    }

    /* Check the connection and type of connection right now and post event to inform activity */
    public void checkWifi(){
        boolean isWifi = false;

        ConnectivityManager cm =
                (ConnectivityManager) app.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnectedOrConnecting();

        if(isConnected)
            isWifi= activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

        Log.d("Connection changed", "conn? " + isConnected + " wifi? " + isWifi);
        EventBus.getDefault().post(new WifiStatusEvent(isConnected && isWifi));
    }
}
