package onewill.onewill.productlisting;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import de.greenrobot.event.EventBus;
import onewill.onewill.R;
import onewill.onewill.data.Product;
import onewill.onewill.productlisting.events.RequestProductsEvent;


public class ProductListingFragment extends Fragment implements ProductListingContract.View{

    @Bind(R.id.product_pager)
    ViewPager mPager;
    ProductListingController controller;
    @Bind(R.id.product_warning_textview)
    TextView warningTextView;
    private List<ProductFragment> fragmentList;
    private ScreenSlidePagerAdapter mPagerAdapter;
    private EventBus eventBus = EventBus.getDefault();
    private boolean reabastecer =false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.productlisting_listing_fragment, container, false);
        ButterKnife.bind(this, rootView);

        mPagerAdapter = new ScreenSlidePagerAdapter(getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        eventBus.post(new RequestProductsEvent()); //request for products
        warningTextView.setVisibility(reabastecer ? View.VISIBLE : View.INVISIBLE);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        controller = new ProductListingController(this);
        fragmentList = new ArrayList<ProductFragment>();
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        controller.onDestroy();
        super.onDestroy();
    }

    @Override
    public void selectProduct(Product selecionado) {
        Toast.makeText(getContext(),"Comprado: "+ selecionado.getTitle(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void waterDispenser() {
        Toast.makeText(getContext(),"Liberar água!", Toast.LENGTH_SHORT).show();
    }

    public void updateProducts(List<Product> products)
    {
        Log.d("Adapter update", "" + (mPagerAdapter==null));
        for (Product product:products) {
            if(fragmentList.size() < products.size()) {
                ProductFragment productFragment = new ProductFragment();
                productFragment.setProduct(product);
                fragmentList.add(productFragment);
            }else{
                fragmentList.get(product.getId()).setProduct(product);
            }
        }
        mPagerAdapter.notifyDataSetChanged();
    }

    public void showWarning()
    {
        reabastecer =true;
       warningTextView.setVisibility(TextView.VISIBLE);

    }


    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentList.get(position);
        }

        @Override
        public int getCount() {
            return fragmentList.size();
        }
    }


}
