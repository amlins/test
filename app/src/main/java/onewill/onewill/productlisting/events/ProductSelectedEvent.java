package onewill.onewill.productlisting.events;

import onewill.onewill.data.Product;

public class ProductSelectedEvent {
    private Product selected;

    public Product getSelected() {
        return selected;
    }

    public ProductSelectedEvent(Product selected) {
        this.selected = selected;
    }
}
