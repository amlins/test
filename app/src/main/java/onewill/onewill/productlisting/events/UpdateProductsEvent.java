package onewill.onewill.productlisting.events;

import java.util.ArrayList;

import onewill.onewill.data.Product;

public class UpdateProductsEvent {
    ArrayList<Product> productList;

    public UpdateProductsEvent(ArrayList<Product> productList) {
        this.productList = productList;
    }

    public ArrayList<Product> getProductList() {
        return productList;
    }
}
