package onewill.onewill.productlisting;

import java.util.List;

import de.greenrobot.event.EventBus;
import onewill.onewill.data.events.ProductShortageEvent;
import onewill.onewill.main.events.ShowAuthenticationEvent;
import onewill.onewill.data.Product;
import onewill.onewill.productlisting.events.ProductSelectedEvent;
import onewill.onewill.productlisting.events.UpdateProductsEvent;
import onewill.onewill.productlisting.events.WaterDispenserEvent;

public class ProductListingController implements ProductListingContract.UserEventHandler {
    private ProductListingContract.View productListingView;
    private EventBus eventBus = EventBus.getDefault();

    public ProductListingController(ProductListingContract.View productView) {
        this.productListingView = productView;
        eventBus.register(this);
    }

    @Override
    public void onEvent(WaterDispenserEvent waterDispenserEvent) {
        productListingView.waterDispenser();
    }

    @Override
    public void onEvent(ProductSelectedEvent productSelectionEvent) {
        eventBus.post(new ShowAuthenticationEvent(productSelectionEvent.getSelected()));
    }

    @Override
    public void onEvent(ProductShortageEvent productShortageEvent) {
        productListingView.showWarning();
    }

    @Override
    public void onEvent(UpdateProductsEvent updateProductsEvent) {
        List<Product> products = updateProductsEvent.getProductList();
        productListingView.updateProducts(products);
    }

    @Override
    public void setView(ProductListingContract.View productListingView) {
        this.productListingView = productListingView;
    }

    @Override
    public void onDestroy() {
        eventBus.unregister(this);
    }

}
