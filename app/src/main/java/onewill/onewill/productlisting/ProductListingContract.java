package onewill.onewill.productlisting;

import java.util.List;

import onewill.onewill.data.events.ProductShortageEvent;
import onewill.onewill.data.Product;
import onewill.onewill.productlisting.events.ProductSelectedEvent;
import onewill.onewill.productlisting.events.UpdateProductsEvent;
import onewill.onewill.productlisting.events.WaterDispenserEvent;

public interface ProductListingContract {
    interface View{
        void selectProduct(Product selecionado);
        void waterDispenser();
        void showWarning();
        void updateProducts(List<Product> products);
    }

    interface UserEventHandler{
        public void onEvent(WaterDispenserEvent waterDispenserEvent);
        public void onEvent(ProductSelectedEvent productSelectionEvent);
        public void onEvent(ProductShortageEvent productShortageEvent);
        public void onEvent(UpdateProductsEvent updateProductsEvent);
        public void setView(View view);
        public void onDestroy();
    }
}
