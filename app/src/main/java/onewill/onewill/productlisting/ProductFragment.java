package onewill.onewill.productlisting;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;
import onewill.onewill.R;
import onewill.onewill.data.Product;
import onewill.onewill.productlisting.events.ProductSelectedEvent;
import onewill.onewill.productlisting.events.WaterDispenserEvent;


public class ProductFragment extends Fragment {

    private Product product;
    private EventBus eventBus = EventBus.getDefault();

    @Bind(R.id.product_title_textview)
    TextView titleTextView;

    @Bind(R.id.product_description_textview)
    TextView descriptionTextView;

    @Bind(R.id.product_price_textview)
    TextView priceTextView;

    @Bind(R.id.product_buy_button)
    Button buyButton;

    @Bind(R.id.product_quantity_textview)
    TextView quantityTextView;

    @OnClick(R.id.product_buy_button)
    public void submit(){
       eventBus.post(new ProductSelectedEvent(product));
    }

    @OnClick(R.id.agua_button)
    public void waterDispenser(){
        eventBus.post(new WaterDispenserEvent());
    }

    public void setProduct(Product product)
    {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.productlisting_product_fragment, container, false);
        ButterKnife.bind(this, rootView);
        if(product != null)
        {
            titleTextView.setText(product.getTitle());
            descriptionTextView.setText(product.getDescription() );
            priceTextView.setText("Preço: R$" + product.getPrice());
            quantityTextView.setText(" ( " + product.getQuantity() +" doses disponíveis)");

            if(product.getQuantity() == 0){
                buyButton.setEnabled(false);
                quantityTextView.setText("Produto indisponível");
                quantityTextView.setTextColor(Color.MAGENTA);
            }
        }
        return rootView;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
