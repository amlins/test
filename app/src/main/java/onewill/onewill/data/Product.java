package onewill.onewill.data;

import java.io.Serializable;

public class Product implements Serializable {
    private String title;
    private String description;
    private double price;
    private int id;
    private int quantity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public int decrementQuantity()
    {
        if(quantity > 0){
            quantity--;
        }
        return quantity;
    }

    public Product(String title, String description, double price, int id, int quantity) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.id = id;
        this.quantity = quantity;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }
}
