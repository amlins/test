package onewill.onewill.data.events;

/**
 * Created by abonet on 03/02/16.
 */
public class ServeProductEvent {

    private int idProduct;

    public int getIdProduct() {
        return idProduct;
    }

    public ServeProductEvent(int idProduct) {
        this.idProduct = idProduct;
    }
}
