package onewill.onewill.data;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import onewill.onewill.data.events.ProductShortageEvent;
import onewill.onewill.data.events.ServeProductEvent;
import onewill.onewill.data.events.EndPurchaseEvent;
import onewill.onewill.productlisting.events.RequestProductsEvent;
import onewill.onewill.productlisting.events.UpdateProductsEvent;

/**
 * Created by abonet on 27/01/16.
 */
public class ProductRepository {
    ArrayList<Product> mProducts;

    public ProductRepository() {
        mProducts = new ArrayList<Product>();
        mProducts.add(new Product("Whey", "Whey consectetur adipiscing elit. Aliquam sed nisi nec dui consectetur fringilla quis eget odio. Ut aliquam massa augue, eget commodo mauris porttitor sit amet. Etiam dapibus dignissim dui, " +
                "cras semper est ac purus efficitur, ut accumsan nunc placerat eget.  " +
                "Sed eleifend justo semper.", 20.50, 0,7));
        mProducts.add(new Product("Carbs", "Whey sabor morango consectetur adipiscing elit. Aliquam sed nisi nec dui consectetur fringilla quis eget odio. Ut aliquam massa augue, eget commodo mauris porttitor sit amet. " +
                "Etiam dapibus dignissim dui, ut accumsan nunc placerat eget." +
                " Cras semper est ac purus efficitur, sed eleifend justo semper.", 25.80, 1, 30));
        mProducts.add(new Product("1Whey", "Whey sabor chocolate " +
                "Etiam dapibus dignissim dui, ut accumsan nunc placerat eget. " +
                "consectetur adipiscing elit. Aliquam sed nisi nec dui consectetur fringilla quis eget odio. Ut aliquam massa augue, eget commodo mauris porttitor sit amet. " +
                "Cras semper est ac purus efficitur, sed eleifend justo semper.", 21.95, 2, 30));
        EventBus.getDefault().register(this);
    }

    public ArrayList<Product> getAll() {
        return mProducts;
    }

    public Product getProduct(int id) {
        return mProducts.get(id);
    }

    public int decrementProduct(int id)
    {
        return mProducts.get(id).decrementQuantity();
    }

    public void onEvent(RequestProductsEvent requestProductsEvent) {

        EventBus.getDefault().post(new UpdateProductsEvent(mProducts));
    }

    public void onEvent(ServeProductEvent serveProductEvent) {

        int id = serveProductEvent.getIdProduct();
        int quantity = mProducts.get(id).decrementQuantity();
        EventBus.getDefault().post(new EndPurchaseEvent());
        if(quantity < 6)
            EventBus.getDefault().post(new ProductShortageEvent());
    }
}
